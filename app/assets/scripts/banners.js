window.banners = {}

Edools.events.subscribe('school', function(e) {
  $('#header-banners').remove();
  json = window.banners[e.split('.').slice(1).join('_')]

  if (json != undefined && json.length > 0) {
    create_headers(json);
  }
});

function create_headers (json) {
  var header_banners = $('<div id="header-banners" class="main-header">');
  header_banners;

  $.each(json, function(i, banner) {
    header_banner = $('<div class="banner">')

    image_container = $('<div class="image-container">')

    var src = "https://cdn.edools.com/themes/552fb1e07268656728010000" + banner.url

    var img = $('<img class="cover-photo">').attr('src', src);
    image_container.append(img);

    if (banner.dark) {
      image_container.append($('<div class="black-overlay">'));
    } else {
      image_container.append($('<div class="semi-black-overlay">'));
    };

    header_banner.append(image_container);

    text_area = $('<div class="text-area container">');
    if (banner.alignment) {
      // text_area.addClass("text-" + banner.alignment)
      text_area.addClass(banner.alignment);
    };

    if (banner.title) {
      var h2 = $('<h2>').html(banner.title)
      if (banner.title_class) {
        h2.addClass(banner.title_class);
      };
      text_area.append(h2);
    };

    if (banner.subtitle) {
      text_area.append($('<p>').html(banner.subtitle));
    };

    if (banner.action && banner.action_url) {
      text_area.append($('<a class="btn-transparent-white">').attr('link-to', banner.action_url).html(banner.action))
    };

    if (banner.small) {
        header_banners.addClass('small-header-banner')
    };

    header_banner.append(text_area);

    header_banners.append(header_banner);

    img.load(function() {
      parent = $(this).parents('.banner:first')
      if ($(this).width() / $(this).height() > parent.width() / parent.height()) {
        $(this).addClass('full-height');
      } else {
        $(this).addClass('full-width');
      };
    });
  });

  $('.app-content').children("[ui-view='content']").prepend(header_banners);

  if (json.length > 1) {
    header_banners.owlCarousel({
      dots: true,
      items: 1,
      slideBy: 1
    });
  };
}
