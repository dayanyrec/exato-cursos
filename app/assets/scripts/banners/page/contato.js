window.banners['page_contato'] = [
  {
    "url": "/assets/images/banners/contato.jpg",
    "title": "FICOU COM DÚVIDAS?<br>FALE COM A NOSSA EQUIPE",
    "alignment": "center",
    "title_class": 'white-static-page-title',
    "dark": true
  }
];

Edools.events.subscribe('school.page.contato', function(e) {
  $('form').find('button').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var data = getFormData($(this).parents('form:first'));

    var name = $('<p>').text(data['name'])
    name.prepend($("<b>").text('Nome: '))

    var email = $('<p>').text(data['email'])
    email.prepend($("<b>").text('E-mail: '))

    var phone = $('<p>').text(data['phone'])
    phone.prepend($("<b>").text('Telefone: '))

    var message = $('<p>').text(data['message'])
    message.prepend($("<b>").text('Mensagem: '))

    var html = [name.prop('outerHTML'), email.prop('outerHTML'), phone.prop('outerHTML'), message.prop('outerHTML')].join('');

    $.ajax({
      url: 'https://mandrillapp.com/api/1.0/messages/send.json',
      type: 'POST',
      dataType: 'json',
      data: {
        'key': 'JAuW8JBFZB3rEWOOvPOrCQ',
        'message': {
          'html': html,
          'subject': 'Contato Site Exato Cursos',
          'from_email': data['email'],
          'from_name': data['name'],
          'to': [
            {
              'email': 'contato@exatocursos.com',
              'name': 'Contato Exato Cursos',
              'type': 'to'
            }
          ],
          'headers': {
            "Reply-To": data['email']
          },
          'track_opens': false,
          'track_clicks': false
        }
      },
    })
    .done(function(response) {
      console.log(response);
    })
  });
});


function getFormData(form){
  var unindexed_array = form.serializeArray();
  var indexed_array = {};

  $.map(unindexed_array, function(n, i){
      indexed_array[n['name']] = n['value'];
  });

  return indexed_array;
}
