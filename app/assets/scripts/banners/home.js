window.banners['home'] = [
  {
    "url": "/assets/images/banners/home1.jpg",
    "title": "PREPARE-SE COM A <b>MELHOR EQUIPE</b> DE JURISTAS DO BRASIL.",
    "subtitle": "OAB, Tribunais, Módulos Jurídicos, entre outros. O único curso online referendado pelo Professor Des. Sylvio Capanema.",
    "alignment": "right"
  }
]
