Edools.events.subscribe('school', function() {
  for (var i = 0; i < 10; i++) {
    setTimeout(set_images, i * 1000);
  };
});

function set_images () {
  $('.image-full').each(function(i, el) {
    $(el).load(function() {
      var parent = $(this).parents('.image:first')
      if (parent.length < 1) {
        parent = $(this).parents('.image-container:first')
      };
      if ($(this).width() / $(this).height() > parent.width() / parent.height()) {
        $(this).addClass('full-height');
      } else {
        $(this).addClass('full-width');
      };
    });
    $(el).load();
  });
};
